var express = require('express');
var _  = require('underscore');
var app = express();
var fs = require("fs");
app.use(express.static('public'));

app.get('/api/listAll', function (req, res) {
   fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
       data = JSON.parse(data);
       res.end( JSON.stringify(data));
   });
})

app.get('/api/listStars/:starsNum', function (req, res) {
	var myStars = req.param('starsNum')
   fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
      var hotels = JSON.parse(data);
      hotels =  _.filter(hotels, function(a){ return a.stars == myStars; });
      console.log(hotels)
      res.end( JSON.stringify(hotels));
   });
})
 

app.use('/css', express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res) {           
  res.sendfile('./public/index.html');        
});

app.listen(8080, function() {
  console.log('App listening on port 8080');
});