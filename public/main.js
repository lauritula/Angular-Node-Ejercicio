angular.module('myapp', []);

function controller('Ctrl', function($scope, $http){
     $http.get('/api/listAll')
        .success(function(data) {
            $scope.list = data;
            console.log(data);
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });
    };
