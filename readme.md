# AlMundo - Ejercicio Frontend

# De que se trata?
Ejercicio Práctico provisto por Recursos Humanos de Almundo, parte del proceso de selección para el puesto de Desarrolador Frontend.
Consiste en una pantalla de resultados de hoteles, y la funcion de filtrarlos segun la cantidad de estrellas, utilizando AngularJS y nodeJS como principales tecnolog´ías.

### Que necesitamos?

Para el correcto funcionamiento del ejercicio, se necesita:
* [node.js](https://nodejs.org/es/)
* [Express](http://expressjs.com/es/)
* [npm](https://www.npmjs.com/)
* [underscore.js](http://underscorejs.org/)


### Cómo lo ejecutamos?

Ejecutando por consola:

```sh
$ cd project
$ npm install
$ node server
```

### Gracias!
María Laura Tula Acosta -- mlaura.tula@gmail.com
